Vaporizer project using chinese pro mini 328 based arduino.

LCD is SSD1306 compatible display, the one I used is
http://www.ebay.com/itm/0-96-inch-SPI-Serial-128-X-64-OLED-Display-Module-for-Arduino-/321766168838?hash=item4aeac21906:g:GHEAAOSwnDZUKhui

It has 4 pins SDA,SCL,VCC and GND. It only works in IIC mode address must be set to 0x3C

The arduino I used is: http://www.ebay.com/itm/MINI-USB-Nano-V3-0-ATmega328P-CH340G-5V-16M-Micro-controller-board-Arduino-/161403910939?hash=item25946c3b1b:g:kEYAAOSwq7JT-wVU

This board came without a bootloader so you'll need a programmer also like the USBTinyISP and arduino software v1.6 at time of writing.
and the CHP340 driver available at http://www.wch.cn/downloads.php?name=pro&proid=65

after driver is intalled load arduino software in tools menu select mini board and make sure 328 is selected for uc and select proper com port.
solder 6 pin programming header and connect usbtiny in arduino software select usbtiny for tools->programmer then click burn bootloader.
you now no longer need to use the usbtiny for programming, and serial debug messages will appear on the usb/serial connection.


Batteries are 18650 cells from an old laptop battery.

also purchased: http://www.ebay.com/itm/New-design-30A-range-Current-Sensor-Module-ACS712-Module-Arduino-module-/251980919194?hash=item3aab3bb19a:g:l2EAAOSwNSxVbYxi
hal effect based current sensor.

and a DCDC converter: http://www.ebay.com/itm/3V-to-5V-1A-DC-DC-Converter-Step-Up-Boost-Module-USB-Charger-for-MP3-MP4-Phone-/261097668334?hash=item3ccaa23eee:g:MuEAAOSwBahVPyz1
for 5v generation. I thought of using the box as a gen purpose moble battery extender as well.

Those parts along with standard 510 connector I push button I had laying around some leds and 3d printed box design
