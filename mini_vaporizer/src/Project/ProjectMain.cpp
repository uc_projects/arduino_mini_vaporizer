/*
 * ProjectMain.cpp
 * chip: mega328p
 * Created: 3/26/2015 6:13:50 PM
 *  Author: Brad Steffy
 
 D6 PWM out DC/DC pin 10
 A0 VTest DC/DC
 A4 SDA
 A5 SCL
 
 ----------------------------
 DC/DC Boost converter
 2 15SQ060 diodes
 1 STD60N3L N channel Mosfet
 1uh inductor 4 turns thick guage wire
 1000uf 16v solid low ESR cap
 
 3.3v 17A rated supply but can't handle 4.51A which sags to 2v
  
 .6ohm Rl
 3.03v B+
 .005v Vind
 .317v Vf  = .634 vf per diode
 2.21v Vl
 3.68A Il 3.74A measured
 
 got 20v unloaded
 */ 

#include "ProjectMain.h"
#include <avr/sleep.h>  // library so it can use various sleep states
#include <EEPROM.h>
#include <avr/io.h>
#include <avr/interrupt.h>

Adafruit_SSD1306 display(4);
#define LINE1 20 //min=16, each line+9
#define LINE2 29
#define LINE3 38
#define LINE4 47
#define LINE5 56

void drawText(uint8_t x, uint8_t y, char *txt, uint8_t tsize){
    //display.clearDisplay();
    display.setTextColor(WHITE);
    display.setTextSize(tsize);
    display.setCursor(x, y);
    display.print(txt);
	//display.display_buffer();	
}

#define BATTERY_STATUS_WIDTH 38
#define BATTERY_STATUS_HEIGHT 15
void drawBatteryStatus(float percent){
	uint8_t fill_width = BATTERY_STATUS_WIDTH * percent;
	//clear battery bar
	for(uint8_t i=2;i<(BATTERY_STATUS_HEIGHT-1);i++){
		display.drawFastHLine(2, i, BATTERY_STATUS_WIDTH-3, BLACK);
	}
		
	display.drawFastVLine(0, 1, BATTERY_STATUS_HEIGHT, WHITE);
	display.drawFastHLine(1, 0, BATTERY_STATUS_WIDTH-1, WHITE);
	for(uint8_t i=2;i<(BATTERY_STATUS_HEIGHT-1);i++){
		display.drawFastHLine(2, i, fill_width-3, WHITE);
	}
	display.drawFastHLine(1, BATTERY_STATUS_HEIGHT, BATTERY_STATUS_WIDTH-1, WHITE);
	display.drawFastVLine(BATTERY_STATUS_WIDTH, 0, BATTERY_STATUS_HEIGHT, WHITE);
	//display.display_buffer();
}

// Sleep Code
void wakeUpNow() {             // here the interrupt is handled after wakeup
	// Wake up actions
	display.ssd1306_command(SSD1306_DISPLAYON);
	delay(50);
}

// Sleep Setup for IDLE mode
void sleepNow() {                      // here we put the arduino to sleep
	display.ssd1306_command(SSD1306_DISPLAYOFF);
	set_sleep_mode(SLEEP_MODE_IDLE);       // sleep mode is set here from the 5 available: SLEEP_MODE_IDLE, SLEEP_MODE_ADC, SLEEP_MODE_PWR_SAVE, SLEEP_MODE_STANDBY,  SLEEP_MODE_PWR_DOWN
	//sleep_enable();                        // enables the sleep bit in the mcucr register so sleep is possible. just a safety pin
	//attachInterrupt(0, wakeUpNow, LOW);  // use interrupt 0 (pin 2) and run function wakeUpNow when pin 2 gets LOW
	sleep_mode();                          // here the device is actually put to sleep!!  THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP
	//sleep_disable();                       // first thing after waking from sleep is disable sleep.
	//detachInterrupt(0);                  // disables interrupt 0 on pin 2 so the wakeUpNow code will not be executed during normal running time.
}

//persistent storage = 1024 bytes
#define battery_ah_addr 0
#define test_float_addr 2
void write_uint16_eeprom(uint16_t v, uint16_t addr){
	uint8_t lowValue = v & 0xFF;
	uint8_t highValue = (v & 0xFF00) >> 8;
	EEPROM.write(addr, lowValue);
	EEPROM.write(addr+1, highValue);
}
uint16_t read_uint16_eeprom(uint16_t addr){
	uint16_t v = EEPROM.read(addr);
	v += EEPROM.read(addr+1) << 8;
	return v;
}
void write_float_eeprom(float v, uint16_t addr){
	eeprom_write_block(&v, &addr, sizeof(v));
}
float read_float_eeprom(uint16_t addr){
	float v;
	eeprom_read_block(&v, &addr, sizeof(v));
	return v;
}

void test_eeprom(){
	//display.clearDisplay();
	drawText(0, LINE1, "Ah: ", 1); //longer strings wrap text
	display.print(read_uint16_eeprom(battery_ah_addr));
	drawText(0, LINE2, "float: ", 1); //longer strings wrap text
	write_float_eeprom(1024.123456, test_float_addr); //displays 1024.12
	display.print(read_float_eeprom(test_float_addr));
	display.display_buffer();	
}

void test_text(){
	//display.clearDisplay();
	drawText(0, LINE1, "123456789123456789123", 1); //longer strings wrap text
	drawText(0, LINE2, "123456789123456789123", 1); //size 1 font is 8 pixels high 6 pixels wide 21 chars long
	drawText(0, LINE3, "123456789123456789123", 1);
	drawText(0, LINE4, "123456789123456789123", 1);
	drawText(0, LINE5, "123456789123456789123", 1);
	display.display_buffer();
}

void test_batt_status(){
	// Clear the buffer.
	display.clearDisplay();
	drawBatteryStatus(1.0);
	display.display_buffer();
	delay(1000);
	
	//display.clearDisplay(); //battery auto clears %bar
	drawBatteryStatus(0.5);
	display.display_buffer();
	delay(1000);
	//display.clearDisplay();
}

/*
    3.2/.15 = 21.333A = 68w
    absolute min = 3.2/30 = .10 ohm = 96w
	0.15 ohm - .5ohm RDA	
	alien wire build https://www.youtube.com/watch?v=w7Z8oUfdSCg
	
	arduino DC/DC converter article: http://www.instructables.com/id/Arduino-based-Switching-Voltage-Regulators/?ALLSTEPS
	
	milliohm meter https://www.youtube.com/watch?v=anE0jDeBuxo
*/

#define ACS712_Vout_Pin A0
float measureCurrent(bool display_result){
/* ACS712ELC-30A Specs
	66mV / A
	5v operation
	40 to 85 degC operation
	35uS power on time
	
	vout = vcc/2 = no current
	vout < vcc/2 = -current
	vout > vcc/2 = +current
	
	(vout - (vcc/2)) / .066 = Imeasure
*/

    // Convert the raw value being read from analog pin	
	uint16_t VoutADC = 2.5; //analogRead(ACS712_Vout_Pin);	
	float Vout = VoutADC * (5.0 / 1023); //5v vcc 10bit adc
	float I = (Vout - 2.5) / .066;
	if(display_result){
		display.clearDisplay();
		drawText(0, LINE1, "Vout: ", 1);
		display.print(Vout);
		drawText(0, LINE2, "I: ", 1);	
		display.print(I);
		display.display_buffer();
	}
	return I;
}


//----------------------- Register setting Code 328p -----------------------------------------------
void setTCCR0B(){
	/*
	TCCR0B - Timer/Counter 0 Control Register B	
	bit           7          6        5       4         3         2         1        0
	name        FOC0A      FOC0B      -       -       WGM02      CS02      CS01     CS00
	set to        0          0        0       0         0         0         0        1
	
		FOC0A = 0     not used in PWM mode
		FOC0B = 0	
		bit 5 = 0
		bit 4 = 0
	
		WGM02 = 0     Fast PWM mode, also see TCCR0A	
		CS02 = 0
		CS01 = 0      no prescaling
		CS00 = 1
	*/
	TCCR0B = 0b00000001;	
}
void setTCCR0A(){
	/*
	TCCR0A - Timer/Counter 0 Control Register A	
	bit           7         6         5         4        3       2        1        0
	name        COM0A1    COM0A0    COM0B1    COM0B0     -       -      WGM01    WGM00
	set to        1         0         0         0        0       0        1        1
	
		COM0A1 = 1    when Timer/Counter 0 (TCNT0) rolls over, set pin OC0A to high
		COM0A0 = 0    when Timer/Counter 0 (TCNT0) equals OCR0A, set pin OC0A to low
	
		COM0B1 = 0    normal port operation, OC0B disconnected
		COM0B0 = 0
	
		bit 3 = 0
		bit 2 = 0
	
		WGM01 = 1     Fast PWM mode, also see TCCR0B
		WGM00 = 1
	*/
	TCCR0A = 0b10000011;	
}
void setADCSRA(){
	/*
	ADCSRA - ADC Control and Status Register A	
	bit          7           6            5          4          3            2           1           0
	name        ADEN        ADSC        ADATE       ADIF       ADIE        ADPS2       ADPS1       ADPS0
	set to       1           0            1          0          1            0           1           1
	
		ADEN = 1     enable ADC
		ADSC = 0     don't start ADC yet
		ADATE = 1    enable ADC auto trigger (i.e. use free running mode)
		ADIF = 0     don't set ADC interrupt flag
		ADIE = 1     enable ADC interrupt
		
		ADPS2 = 0
		ADPS1 = 1    1 MHz clock / 8 = 125 kHz ADC clock
		ADPS0 = 1
	*/
	ADCSRA = 0b10101011;	
}
void setADCSRB(){
	/*
	ADCSRB - ADC Control and Status Register B	
	bit         7           6           5           4           3         2           1           0
	name        -          ACME         -           -           -       ADTS2       ADTS1       ADTS0
	set to      0           0           0           0           0         0           0           0
	
		bit 7 = 0
		ACME = 0     don't enable analog comparator multiplexer
		bit 5 = 0
		bit 4 = 0
		bit 3 = 0
		ADTS2 = 0
		ADTS1 = 0    free running mode
		ADTS0 = 0
	*/
	ADCSRB = 0b00000000;
}
void setADMUX(){
	/*
	ADMUX - ADC Multiplexer Selection Register	
	bit          7           6          5         4        3         2          1          0
	name       REFS1       REFS0      ADLAR       -       MUX3      MUX2       MUX1       MUX0
	set to       0           1          1         0        0         1          0          0
	
		REFS1 = 0    use AVCC for reference voltage
		REFS0 = 1
		
		ADLAR = 1    left justify ADC result in ADCH/ADCL
		
		bit 4 = 0
		
		MUX3 = 0     use PC0/ADC0 (pin 23) for input
		MUX2 = 0
		MUX1 = 0
		MUX0 = 0
	*/
	ADMUX = 0b01100000;
}
void startADC(){
	ADCSRA |= (1 << ADSC);		// start ADC	
}
void test_AnalogInPWMOut(){
	//DDRD |= (1 << 10);		// set PD6/OC0A (pin 10) for output	

	setADMUX();
	setADCSRA();
	setADCSRB();
	setTCCR0A();
	setTCCR0B();
	sei();						// enable interrupts
	startADC();
    //while (1) { }	
	//return(0);					// should never get here, this is to prevent a compiler warning
}
///////////////////////////////////////////////////////////////////////////////////////////////////
ISR(ADC_vect) {
	OCR0A = ADCH;				// assign contents of ADC high register to output compare register
}

void setup(){
	pinMode(6, OUTPUT);	  //set output PWM D6 pin high
	digitalWrite(6, HIGH);
	//delay(5000);
	delay(180);          //delay keeps screen from being random pixels
	Serial.begin(9600);
	delay(20);          //allow serial port to stabilize
	//Serial.println("Hello World");
	display.begin();  // initialize with the I2C addr 0x3C
	
	// Show image buffer on the display hardware.
	// Since the buffer is initialized with an Adafruit splash screen
	display.display_buffer();
	delay(1000);

	//test_batt_status();
	//test_text();
	//test_eeprom();
	delay(1000);
	//sleepNow();
	test_AnalogInPWMOut();
}

void loop(){
	//float I = measureCurrent(true); //measure and display result
	Serial.println("1010");
	int t = OCR0A;
	display.clearDisplay();
	drawText(0, LINE1, "PWM: ", 1);
	display.print(t);
	display.display_buffer();
	delay(200);
}
