/*

  Regulated DC Positive Voltage Booster
  Created 12/17/2008 by Amp <amplificar@gmail.com>
  Modified 11/28/2011 to be compatible with the newest Arduino IDE 0023
  
  
  If powered by the Arduino 5V, connected by USB it should be reasonably safe to
  build and operate this circuit if you follow the guidelines provided below.
  There is some risk of personal injury. (read the disclaimer)
  
  Voltage Input:  +1VDC minimum, 12V maximum
  Voltage Output: Vin to +60VDC maximum
  Output Power:   250mW maximum (0.25W), dependent on Q1, L1 and voltage source's internal resistance
  
  Using 5V from the Arduino, at 250mW the input current drain is 50mA (0.25W/5V=0.05A).
  10V circuit output, max current is 25mA (0.25W/10V=0.025A)
  20V output, 12.5mA max (0.25W/20=0.0125A)
  40V output, 6.25mA max (0.25W/40=0.00625A)
  
  THE #1 RULE: Do not charge large capacitors to high voltages.
  Capacitors can discharge instantly, and that can be hazardous. 
  
  Voltages exceeding 60V may be possible
  * Wear safety glasses
  * Don't lean over your circuit while it's powered
  * Don't wear loose fitting jewelry
  * Keep liquids, flammable or loose debris away from the circuit
  * Do not leave the circuit operating unattended
  * Never power the circuit from negative voltages, AC or directly from a wall outlet
  
  Circuit Theory:
  Inductors resist changes in current. When Q1 is active, a larger current begins to flow through L1 to GND.
  When Q1 is switched off, the current through L1 trys to remain the same as it was, causing an increased voltage.
  By varying the PWM duty cycle, the current through L1 can be controlled.
  A larger duty cycle makes a larger output voltage.
  
  Software Summary:
  Regulation is important when the output load isn't constant.
  If the duty cycle remains the same, and the load increases, the output voltage will be reduced.
  The software senses changes in the output voltage, and adjusts the PWM to compensate.
  The output voltage depends on load and input voltage, which are "unknown" variables, so the
  mathematical relationship between PWM duty cycle and output voltage are "guessed" by the program.
  If the input voltage and output load were constant, the Arduino analog input wouldn't be necessary.
  With regulation, almost any load and input voltage can be used to make an accurate output voltage.
  
  Modification Theory:
  An "unregulated" positive or negative voltage booster can be built with a 555 timer, instead of the Arduino.
  A negative voltage booster is possible, but Arduino analog inputs need positive voltages.
   Sensing and controlling a negative voltage with the Arduino can probably be done with a depletion mode MOSFET/IGFET.
   That would require negative "Aout" tied to the gate of the dep. MOSFET, with +5VDC (current limited) tied to the source.
   With proper biasing, -1VDC would be +4VDC, -2.5VDC = +2.5VDC, -5VDC = 0VDC, so invert the measurement (1024-measurement)
   This is an untested suggestion/idea. I'd be surprised if this is all there was to it.
  
  Instructions:
  *Follow the safety rules above, before proceeding.
  
    Assembly
  1) Assemble and check the circuit components, without the Arduino connected
  
    Power Up Check (DO NOT connect PWM pin 9, or ADC pin 0 until instructed to)
  2) Connect Arduino GND to circuit GND (this is called common ground)
  3) Connect Arduino 5V to circuit voltage input (the voltage input of L1)
  4) Measure circuit Voltage Output is less than 5V (minus the D1 voltage drop), and more than 0V (check inductor)
  5) Measure circuit analog out (Aout) voltage is less than 400mV (5V/15=0.333mV, Aout is a 15:1 voltage divider)
  
    Power Down Check (still, do not connect pin 9 or 0)
  6) Disconnect 5V, your circuit output voltage will gradually fall to 0V
     Never disconnect GND, unless every other wire to the Arduino is already disconnected.
     (always connect GND first, and disconnect it last)
  7) Repeat the power up check again, and then move on to Operation.
  
    Operation
  8) Choose a low voltage in the setup routine below, such as 9 (where "power_level=SetVoltage(v)" chooses the voltage)
  9) Connect ADC pin 0 to circuit Aout (between the 1.5M and 100K resistors, where shown in the schematic)
     Make sure pin 0 is connected correctly, or your output voltage wont be regulated.
  10) Attach your Voltmeter to circuit Vout
  11) Watch your Voltmeter while you connect pin 9 to the circuit PWM (disconnect pin 9 if the voltage is wrong)

  You can turn off the voltage booster by disconnecting pin 9 from the PWM
  All power can be shut off by disconnecting pin 9 (first), and disconnecting Arduino 5V
  Do not disconnect GND. (see step 6)
  
    TROUBLESHOOTING
  Shut off the circuit before touching, removing or replacing components or wires (see above, and step 6)
  
  P) At step 4, the voltage wasn't between 0V and 5V
     Check diode D1 is inserted with the correct polarity, your inductor is securely connected and conducting,
     NPN transistor Q1 is connected according to the schematic (collector to L1, emitter to GND)
  P) At step 4, the voltage was 5V
     Check your diode D1 is inserted correctly (5V minus its voltage drop, is what you should be measuring at Vout)
  P) At step 4, the voltage was above 5V
     You skipped my instructions and wired arduino pins 0 and 9 already, shame on you.
  
  P) At step 5, the analog out voltage was above 400mV
     Your resistor voltage divider is not wired correctly (check 1.5M and 100K resistors weren't swapped)
  P) At step 5, the analog out was 0V (or open and floating)
     Check the voltage divider is wired to the circuit properly, and the resistors are not "open" (damaged)
  P) At step 5, the analog out voltage was changing constantly or I got no reading
     Make sure the Arduino GND is connected to the circuit GND, and see the above problem about open/floating voltage.
  
  P) After step 9, the voltage dropped slightly
     Your inductor doesn't have enough turns (A PWM frequency of 62KHz isn't high enough)
  P) After step 9, the voltage increased only slightly
     The inductor doesn't have enough turns, or your transistor (Q1) isn't working right
  P) After step 9, the voltage increased beyond the chosen voltage by more than 1V
     Ensure your load resistor is 100K, filter capacitor is between 0.1uF (100nF) and 47uF
     or reduce the SetVoltage() 12.41 multiplier.
  P) After step 9, nothing happened (the voltage didn't change)
     Your inductor wont work try another one, or transistor Q1 isn't working right
  
  P) Different circuit output voltage are off by a relatively constant amount
     Adjust the 12.41 multiplier constant in the SetVoltage() function
     
  P) The circuit has poor regulation, it varys by more than half a volt
     Regulation needs a minimum load and filter capacitance, however at high voltages
     the regulation wont work as accurately.
     At high loads, the output voltage will decrease.
     Check your circuit analog out (Aout) is your output voltage divided by 15.
        -there might be a problem with your resistor voltage divider.
     Or, try a different inductor to see if that's limiting the output.
     If you rapidly change the output load, the output voltage will bounce around.
  
  P) The circuit wont generate a voltage above 6V.
     Try a different inductor, check your Q1 transistor is wired correctly, replace D1
     
*/

//#include <wiring_private.h>
// included for access to cbi(), sbi() macros, and TCCR1B register definitions
// Nov, 2011 - commented out wiring_private.h and copied the macros here
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif


#include <avr\wdt.h>
// access to watchdog timer macros

unsigned int power_level = 0;
// The target regulated voltage, default power level 0V (output will be input voltage +/-1V)
// Do not change this definition (change the SetVoltage(v) assignment in setup, below)

unsigned int SetVoltage(double voltage);
// Calculate the desired analog input reading, for a given voltage

signed int duty = 0; // used in the main program loop
// it's assigned to the analog output, to control the output voltage.
// the relationship between duty cycle and voltage depends on output load and input voltage.
// since neither the load or input are assumed, the program estimates/seeks
// the correct duty cycle by sensing the output voltage and making small adjustments.
// A greater duty cycle makes a greater voltage.

void setup() {
  // I use pin 13 to indicate power, with an LED. (like the older revisions had built-in)
  // For my rev.c arduino, I put an LED between pin 13 and GND (outputs only source 20mA).
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  
  // The following code increases the PWM frequency
  // This might have an effect on delay(n) and millis() functions.
  // The ideal frequency depends on the inductor, but it'll only be important if you need alot more power.
  // My earlier tests indicated 20-300KHz are usable (depending on the inductor).
  // (If 1KHz is used, the inductor will make a buzzing sound.)
  
#if defined(__AVR_ATmega168__)
  // 62.5KHz PWM for the ATMega168 -> only on Arduino pins 9 and 10
  
  // set prescaler to 1
  // (sbi means "set bit register", cbi means "clear bit register")
  cbi(TCCR1B, CS12);
  cbi(TCCR1B, CS11);
  sbi(TCCR1B, CS10);
  
  // set fast PWM
  cbi(TCCR1B, WGM13);
  sbi(TCCR1B, WGM12);
  // with fast PWM, the frequency is (CLK/256*prescaler) = 16MHz/256 = 62.5KHz
  // with slow PWM, it is half that speed (31KHz)
#else
  // 22KHz for the ATMega8 (this is a low frequency, so fewer inductors might work with it)
  TCCR2 = ((TCCR2 & ~0x07) | 0x01);
  TCCR1B = ((TCCR1B & ~0x07) | 0x01);
#endif
  
  
  // enable the watchdog timer (a failsafe reset: if the program freezes, the voltage booster will shut off)
  // the Arduino will reset if this timer isn't cleared in less than 400ms.
  wdt_enable(400);
  
  
  // set the voltage you want the program to maintain (any decimal value greater than your input voltage)
  power_level = SetVoltage(9);
  // Do not exceed 24V without taking special precautions.
  // Do not exceed 60V with the circuit/schematic provided with this software. (75V or more will damage the arduino)
  // Do not exceed 250mW (aka 1/4W, aka quarter watt) of power. (MAX: 5V 50mA, 9V 28mA, 12V 21mA, 24V 10mA, 48V 5mA)
}

void loop() {
  unsigned int measure = analogRead(0);
  
  wdt_reset(); // watchdog timer reset (see above)
  
  if (measure>power_level+18) {
    // "panic" if the measured voltage is more than 1V above where it should be,
    // cut power by 50% to respond quickly.
    duty=duty/2;
    
  } else if (measure>power_level) {
    // decrease duty cycle to compensate for smaller load
    duty=max(0,duty-1);
    
  } else if (measure<power_level) {
    // increase duty cycle to compensate for larger load
    duty=min(191,duty+1);
    // Without the 191 limit, it can cause regulation to fail at high loads or high voltages.
    // Only modify this limit if you're using an improved version of the schematic I've given.
    
  } else {
    return;
  }
  
  analogWrite(10,duty);
  
  // If you have HF noise problems, you might want to add a small microseconds delay here to see if it helps.
  // Delays can make voltage regulation worse, by reacting slower to changes in the output load.
  
  // If you have regulation problems, make sure your output filter capacitor is at least 0.1uF.
  // As your circuit's load changes, the capacitor will slow down output fluctuations.
  
}

// Calculate the desired analog input reading, for a given voltage
unsigned int SetVoltage(double voltage) {
  if (voltage>60) {
    return 0; // do not exceed 60V
  }
  
  // This function converts the voltage value (0 to 5V reading; 0 to 60V actual), into a value for an analog input (from 0 to 1024)
  // If your resistor voltage divider is not perfectly 15:1 you'll need to change the formula below
  
  // with ideal resistors and a perfect 5V Analog Reference voltage (AREF), an analog input value of 13.65 would be 1V
  
  return (voltage*12.8);
  // ideal ADC value, per volt     = voltage*(1024/5)/(15/1) = 13.65
  // the real value for my circuit = voltage*(204.8)/(16.5)  = 12.41
  
  // 16.5 was my measured resistor voltage divier ratio.
  // I increased my calculated 12.41 constant to 12.8, by trial and error, in order to get closer to the target voltage
  // My results were +/-0.2V, at a 0.1mA (100Kohm) load when powered by the Arduino.
}
